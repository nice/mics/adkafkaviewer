# Area detector Kafka viewer

## Dependencies

The code was written using Python 3.8.

### CentOS 7 Python 3.8 installation

By default CentOS 7 machines does not have Python 3.8, follow the steps below to install it:

```bash
wget https://www.python.org/ftp/python/3.8.12/Python-3.8.12.tgz
tar xvf Python-3.8.12.tgz
cd Python-3.8*/
./configure --enable-optimizations
sudo make altinstall
# Checking the installation:
python3.8 --version
# Check that pip3.8 was also installed
pip3.8 --version
```
### Python libraries

Necessary Python libraries to run the AD Kafka viewer application:

```bash
pip3.8 install pyqtgraph confluent-kafka ess-streaming-data-types
```

## Usage

```bash
python3.8 ad-kafka-viewer.py kafka_server_ip_or_hostname:9092 your_topic
```
