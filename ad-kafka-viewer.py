import time
from time import perf_counter

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore

import numpy as np

import logging
import argparse

from confluent_kafka import Consumer, TopicPartition, KafkaException, KafkaError
from streaming_data_types import deserialise_ADAr

# Parsing the arguments section
parser = argparse.ArgumentParser(description='EPICS Area Detector Kafka plugin Viewer.')
parser.add_argument('broker',metavar='address',help='IP or hostname of the machine that runs the Kafka broker.')
parser.add_argument('topic',metavar='topic', help='Name of the Kafka topic that this viewer will assign to.')
args = parser.parse_args()
BROKER = args.broker
TOPIC = args.topic

##
# Setting the log system
logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s')
logging.getLogger().setLevel(logging.INFO)

##
# Connecting to the Kafka broker according to the user parameters
consumer = Consumer({
        "bootstrap.servers": BROKER,
        "group.id": f"consumer-{time.time_ns()}",
        "auto.offset.reset": "latest",
    })

logging.info(f"Substribing to Kafka topics: {[TOPIC]}")
consumer.subscribe([TOPIC])

##
# Setting the display
#
app = pg.mkQApp("ImageItem Example")

## Create window with GraphicsView widget
win = pg.GraphicsLayoutWidget()
win.show()  ## show widget alone in its own window
win.setWindowTitle('AD Kafka plugin viewer')
view = win.addViewBox()

## lock the aspect ratio so pixels are always square
view.setAspectLocked(True)

## Create image item
img = pg.ImageItem(border='w')
view.addItem(img)

## Set initial view bounds
view.setRange(QtCore.QRectF(0, 0, 600, 600))

updateTime = perf_counter()
elapsed = 0

timer = QtCore.QTimer()
timer.setSingleShot(True)
# not using QTimer.singleShot() because of persistence on PyQt. see PR #1605

def updateData():
    global img, data, updateTime, elapsed
    msg = consumer.poll(timeout=1)
    if msg is None: 
        pass
    elif msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            # End of partition event
            error_string = f"Topic: {msg.topic()}; Partition: {msg.partition()}; reached end at offset {msg.offset()}"
            logging.error(error_string)
            sys.stderr.write(error_string)
        elif msg.error():
            logging.error(f"{KafkaException(msg.error())}")
            raise KafkaException(msg.error())
    else:
        # If no errors decode the message into an image
        result = deserialise_ADAr(msg.value())
        img.setImage(result.data.T)

    timer.start(1)
    
    now = perf_counter()
    elapsed_now = now - updateTime
    updateTime = now
    elapsed = elapsed * 0.9 + elapsed_now * 0.1

    # print(f"{1 / elapsed:.1f} fps\r")
    
timer.timeout.connect(updateData)
updateData()

logging.info("Start the screen's thread")
pg.exec()

logging.info("Closing the Kafka consumer")
consumer.close()
